module.exports = {
  devServer: {
    https: true
  },
  lintOnSave: false
};
