import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import * as firebase from 'firebase'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
 

import 'bootstrap/dist/css/bootstrap.min.css';

library.add(fas)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

const firebaseConfig = {
  apiKey: "AIzaSyA3qeIvzN42hgzPrZpa4yVHv25tz0mE_2E",
  authDomain: "fir-innojam.firebaseapp.com",
  databaseURL: "https://fir-innojam.firebaseio.com",
  projectId: "fir-innojam",
  storageBucket: "fir-innojam.appspot.com",
  messagingSenderId: "1037924168495",
  appId: "1:1037924168495:web:a65820dd1aea5affe7a7da"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

//Observe changes on the user
firebase.auth().onAuthStateChanged(user => {
  store.dispatch("fetchUser", user);
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
