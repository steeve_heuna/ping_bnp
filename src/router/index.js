import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Chatbot from '@/components/chatbot/Chatbot.vue'
import Main from '@/Main.vue'
import MailForm from '@/components/template/MailForm.vue'
import MailForm2 from '@/components/template/MailForm2.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Main',
    component: Main
  },
 
  {
    path: '/mailform',
    name: 'MailForm',
    component: MailForm
  },
  {
    path: '/mailform2',
    name: 'MailForm2',
    component: MailForm2
  },
  {
    path: '/home',
    name: 'Home',
    component: Home
  },
  {
    path: '/chatbot',
    name: 'Chatbot',
    component: Chatbot
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
